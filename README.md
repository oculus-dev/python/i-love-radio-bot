# ![i-love-radio-logo](i-love-radio-logo-32x32.png) I Love Radio Bot 
A Discord Bot that plays the I Love Radio stream.
## Installation
**[Python 3.8.0](https://www.python.org/downloads/release/python-380/) and
[ffmpeg](https://ffmpeg.org/download.html) is required!**

install [git](https://git-scm.com/downloads) and do ``git clone https://gitlab.com/oculus-dev/python/i-love-radio-bot.git``\
or download the [zip](https://gitlab.com/oculus-dev/python/i-love-radio-bot/-/archive/master/i-love-radio-bot-master.zip) and unzip it with [7-Zip](https://www.7-zip.org/).

```bash
# use python3 or python depends on the installation

# install pip
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

# download the requirements
python -m pip install -r requirements.txt
```

## Usage
Create a file called **token.txt** and paste your token in it
```bash
# use python3 or python depends on the installation
python bot.py
```

## Docker

If docker is installed you can build an image and run this as a container.

```
docker build -t I-Love-Radio-Bot-image  .
```

Once the image is built, I Love Radio can be used by running the following:

```
docker run --rm -t -v .:/opt/bot:rw I-Love-Radio-Bot-image -d
```

The optional ```--rm``` flag removes the container filesystem on completion to prevent cruft build-up.\
See: https://docs.docker.com/engine/reference/run/#clean-up---rm

The optional ```-t``` flag allocates a pseudo-TTY which allows colored output.\
See: https://docs.docker.com/engine/reference/run/#foreground

The optional ```-v``` flag allows to save the video on your Filesystem.\
See: https://docs.docker.com/storage/volumes/

The optional ```-d``` Run's container in background and print container ID\
See: https://docs.docker.com/engine/reference/commandline/run/
