#!/usr/bin/python3
# coding=utf-8

# Default Imports
import asyncio
import json
import time
import codecs

# custom Imports
import discord
from discord.utils import get
import requests
from colorama import *


def time_HMS():
    return str(time.strftime("%H:%M:%S"))


def time_date():
    return str(time.strftime("%d.%m.%Y"))


def log(message, log_type="INFO", log_file="bot.log"):
    log_types = {
        "INFO": Style.BRIGHT + Fore.WHITE,
        "DEBUG": Style.BRIGHT + Fore.YELLOW,
        "WARNING": Style.BRIGHT + Fore.RED,
        "ERROR": Style.DIM + Fore.RED,
    }
    message = '[' + time_date() + " " + time_HMS() + " " + str(log_type) + "]: " + str(message)

    with codecs.open(log_file, "a", "utf-8") as log_file:
        log_file.write(message + "\n")
        log_file.close()

    print(log_types[log_type] + message)


class I_Love_Radio_bot(object):
    def __init__(self, token, url, channels, logs, volume=0.07):
        self.bot = discord.Client()
        self.token = token
        self.url = url
        self.volume = volume
        self.channels = channels
        self.logs = logs

    def run(self):
        @self.bot.event
        async def on_voice_state_update(member, before, after):
            guild = member.guild
            if str(guild.id) in self.channels and not member.id is self.bot.user.id:
                channel = self.bot.get_channel(self.channels[str(guild.id)])
                if str(guild.id) in self.logs and not before.channel is after.channel:
                    if before.channel is channel:
                        left = {
                            "username": "I Love Radio",
                            "embeds": [
                                {
                                    "title": "Left:",
                                    "color": 16711680,
                                    "fields": [
                                        {
                                            "name": "Username:",
                                            "value": str(member.mention),
                                            "inline": True
                                        },
                                        {
                                            "inline": True,
                                            "name": "User ID:",
                                            "value": str(member.id)
                                        }
                                    ],
                                    "footer": {
                                        "text": time_date() + " " + time_HMS()
                                    },
                                    "thumbnail": {
                                        "url": str(member.avatar_url)
                                    }
                                }
                            ],
                            "avatar_url": str(self.bot.user.avatar_url)
                        }
                        requests.post(url=self.logs[str(guild.id)], json=left)
                    elif after.channel is channel:
                        joined = {
                            "username": "I Love Radio",
                            "embeds": [
                                {
                                    "title": "Joined:",
                                    "color": 65321,
                                    "fields": [
                                        {
                                            "name": "Username:",
                                            "value": str(member.mention),
                                            "inline": True
                                        },
                                        {
                                            "inline": True,
                                            "name": "User ID:",
                                            "value": str(member.id)
                                        }
                                    ],
                                    "footer": {
                                        "text": time_date() + " " + time_HMS()
                                    },
                                    "thumbnail": {
                                        "url": str(member.avatar_url)
                                    }
                                }
                            ],
                            "avatar_url": str(self.bot.user.avatar_url)
                        }
                        requests.post(url=self.logs[str(guild.id)], json=joined)
                if before.channel or after.channel is channel:
                    members = len(channel.members)
                    voice = get(self.bot.voice_clients, guild=channel.guild)
                    if voice and voice.is_connected and voice.channel == channel:
                        members = members - 1
                    if members is 0:
                        if voice and voice.is_connected:
                            await voice.disconnect()
                        if str(guild.id) in self.logs:
                            stopped = {
                                "username": "I Love Radio",
                                "embeds": [
                                    {
                                        "title": "Stopped:",
                                        "color": 0,
                                        "footer": {
                                            "text": time_date() + " " + time_HMS()
                                        },
                                        "description": "I Stopped Playing music"
                                    }
                                ],
                                "avatar_url": str(self.bot.user.avatar_url)
                            }
                            requests.post(url=self.logs[str(guild.id)], json=stopped)
                    else:
                        if not (voice and voice.is_connected):
                            voice = await channel.connect()
                            await voice.disconnect()
                            voice = await channel.connect()
                            voice.play(discord.FFmpegPCMAudio(url))
                            voice.source = discord.PCMVolumeTransformer(voice.source)
                            voice.source.volume = self.volume
                            if str(guild.id) in self.logs:
                                playing = {
                                    "username": "I Love Radio",
                                    "embeds": [
                                        {
                                            "title": "Playing:",
                                            "color": 16777214,
                                            "footer": {
                                                "text": time_date() + " " + time_HMS()
                                            },
                                            "description": "I am Playing"
                                        }
                                    ],
                                    "avatar_url": str(self.bot.user.avatar_url)
                                }
                                requests.post(url=self.logs[str(guild.id)], json=playing)

        @self.bot.event
        async def on_ready():
            await self.bot.change_presence(status=discord.Status.online,
                                           activity=discord.Game(name='iloveradio.de', type=2))
            init()
            print(Fore.GREEN + Style.BRIGHT + "\n" +
                  " =======- " + str(time_date()) + " -=======\n" +
                  "  Bot Name: " + str(self.bot.user.name) + '#' + str(self.bot.user.discriminator) + "\n" +
                  "  Bot ID: " + str(self.bot.user.id) + "\n" +
                  "  Discord Version: " + str(discord.__version__) + "\n" +
                  " ========- " + str(time_HMS()) + " -========" +
                  Style.RESET_ALL + "\n")

        self.bot.run(self.token)


# run
if __name__ == '__main__':
    try:
        token = open(r'token.txt', 'r').read()
    except FileNotFoundError:
        log("you need to create a text file called token.txt with the bot token", "ERROR")
    except UnicodeDecodeError:
        log("token.txt has a false Decoding", "ERROR")
    else:
        url = 'http://stream01.iloveradio.de/iloveradio1.mp3'
        try:
            channels = json.loads(open("channels.json", "r").read())
        except json.decoder.JSONDecodeError:
            log("your channels.json in invalid", "ERROR")
        except UnicodeDecodeError:
            log("channels.json has a false Decoding", "ERROR")
        except FileNotFoundError:
            print("you need to create a file called channels.json with the content:\n"
                  "{\n"
                  '    "503969457945837589": 562699711275073566\n'
                  "}\n"
                  '//   server id: channel id\n'
                  , "ERROR"
                  )
        else:
            try:
                logs = json.loads(open("logs.json", "r").read())
            except json.decoder.JSONDecodeError:
                log("your logs.json in invalid", "ERROR")
            except UnicodeDecodeError:
                log("logs.json has a false Decoding", "ERROR")
            except FileNotFoundError:
                log("you need to create a file called logs.json with the content:\n"
                    "{\n"
                    '      "503969457945837589": "https://discordapp.com/api/webhooks/{webhook.id}/{webhook.token}"\n'
                    "}\n"
                    '//    "server id": "webhook"'
                    , "ERROR"
                    )
            else:
                try:
                    I_Love_Radio_bot(token, url, channels, logs).run()
                except discord.errors.LoginFailure:
                    log("your token is invalid", "ERROR")
