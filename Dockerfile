FROM python:3.6-stretch
WORKDIR /opt/bot
COPY requirements.txt .
RUN apt-get update && apt-get install -y ffmpeg && pip install -r requirements.txt
CMD ["python", "bot.py"]